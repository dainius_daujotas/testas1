package testas.mbeans;

import java.io.Serializable;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.DataModel;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import testas.entities.EntityId;
import testas.mbeans.util.EntitiesDataModel;
import testas.mbeans.util.JsfUtil;
import testas.services.EntityFacadeLocal;

public abstract class EntityController<T extends EntityId> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7791111183104724547L;

	private static final Logger LOGGER = Logger.getLogger(EntityController.class.getName());

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("/Bundle");

	private EntitiesDataModel<T> items = null;
	private final Class<T> entityClass;

	private int selectedItemIndex;

	private boolean inlineEditingEnabled = false;

	private T selected;
	private T editing;

	public EntityController(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	// State properties

	public T getSelected() {
		// if (current == null) {
		// current = newEntityInstance();
		// }
		return selected;
	}

	public void setSelected(T newVal) {
		selected = newVal;
	}

	public boolean isEmptySelection() {
		return selected == null;
	}

	public boolean isSelectionEmpty() {
		return getSelected() == null || getSelected().getId() == null;
	}

	public boolean isInlineEditingEnabled() {
		return inlineEditingEnabled;
	}

	public void setInlineEditingEnabled(boolean inlineEditingEnabled) {
		this.inlineEditingEnabled = inlineEditingEnabled;
	}

	public T getEntity(java.lang.Long id) {
		return getFacade().find(id);
	}

	public DataModel<T> getItems() {
		if (items == null) {
			items = new EntitiesDataModel<T>(getFacade());
			if (selectedItemIndex >= 0)
				items.setRowIndex(selectedItemIndex);
		}
		return items;
	}

	public T getEditing() {
		return editing;
	}

	public void setEditing(T editing) {
		this.editing = editing;
	}

	// Actions
	public String prepareList() {
		if (editing!=null) editing = null;
		recreateModel();
		return "List";
	}

	public String prepareView() {
		return "View";
	}

	public String prepareCreate() {
//		setSelected();
		setEditing(newEntityInstance());
		return "Create";
	}
	public String prepareEdit() {
		setEditing(getSelected());
		return "Edit";
	}

	public void prepareInlineCreate() {
		setSelected(newEntityInstance());
		setEditing(getSelected());
		items = new EntitiesDataModel<T>(getEditing(), getFacade());
		items.setRowIndex(0);
		inlineEditingEnabled = true;
	}
	public void prepareInlineEdit() {
		setEditing(getSelected());
		inlineEditingEnabled = true;
		
	}


	public String create() {
		return createEntity() ? prepareCreate() : null;
	}


	public String update() {
		return updateEntity() ? "View" : null;
	}

	public String destroy() {
		performDestroy();
		recreateModel();
		return "List";
	}

	public String destroyAndView() {
		selectedItemIndex = items.getRowIndex();
		performDestroy();
		recreateModel();
		updateCurrentItem();
		if (selectedItemIndex >= 0) {
			return "View";
		} else {
			// all items were removed - go back to list
			recreateModel();
			return "List";
		}
	}

	private void performDestroy() {
		try {
			getFacade().remove(selected);
			JsfUtil.addSuccessMessage(BUNDLE.getString(entityClass.getSimpleName() + "Deleted"));
		} catch (Exception e) {
			JsfUtil.addErrorMessage(e, BUNDLE.getString("PersistenceErrorOccured"));
		}
	}

	private void updateCurrentItem() {
		int count = getFacade().count();
		if (selectedItemIndex >= count) {
			// selected index cannot be bigger than number of items:
			selectedItemIndex = count - 1;
		}
		if (items != null) {
			items.setRowIndex(selectedItemIndex);
		}
		setSelected(selectedItemIndex >= 0
				? getFacade().findRange(selectedItemIndex, 1).get(0)
				: null);
	}

	

	public void cancelInlineEdits() {
		if (getEditing() != null && (getEditing().getId() == null || -1l == getEditing().getId())) {
			recreateModel();
		}
		setEditing(null);
		inlineEditingEnabled = false;
		JsfUtil.addSuccessMessage(BUNDLE.getString(entityClass.getSimpleName()),
				BUNDLE.getString(entityClass.getSimpleName() + "Canceled"));
	}

	public void updateInlineEdits() {
		boolean isNew = isNewEditingItem();
		boolean updated = isNew ? createEntity() : updateEntity();
		if (updated) {
			if (isNew)
				recreateModel();
			inlineEditingEnabled = false;
		} else {
			selected = getEditing();
			inlineEditingEnabled = true;
		}
	}
	
	public void refresh() {
		setSelected(null);
		recreateModel();
	}

	// Event callbacks

	public void onRowSelect(SelectEvent event) {
		if (inlineEditingEnabled) {
			RequestContext.getCurrentInstance().execute("PF('rowEditingConfirm').show();");
			inlineEditingEnabled = false;
		}
	}

	// Utility methods
	public boolean isNewEditingItem() {
		return getEditing().getId() == null || getEditing().getId() < 0;
	}

	protected boolean createEntity() {
		try {
			if (getEditing().getId() != null)
				getEditing().setId(null);
			getFacade().create(getEditing());
			JsfUtil.addSuccessMessage(BUNDLE.getString(entityClass.getSimpleName()),
					BUNDLE.getString(entityClass.getSimpleName() + "Created"));
			return true;
		} catch (Exception e) {
			JsfUtil.addErrorMessage(e, BUNDLE.getString("PersistenceErrorOccured"));
			return false;
		}
	}

	public boolean updateEntity() {
		try {
			getFacade().edit(getEditing());
			JsfUtil.addSuccessMessage(BUNDLE.getString(entityClass.getSimpleName()),
					BUNDLE.getString(entityClass.getSimpleName() + "Updated"));
			return true;
		} catch (Exception e) {
			JsfUtil.addErrorMessage(e, BUNDLE.getString("PersistenceErrorOccured"));
			return false;
		}
	}

	private void recreateModel() {
		items = null;
	}

	public boolean inlineEditable(T item) {
		return selected != null && item != null && inlineEditingEnabled && item.getId() != null
				&& item.getId().equals(selected.getId());
	}

	public boolean inlineNewEditable(T item) {
		return selected != null && item != null && inlineEditingEnabled && item.getId() != null
				&& item.getId().equals(selected.getId()) && -1l == item.getId();
	}

	protected T newEntityInstance() {
		try {
			T ret = entityClass.newInstance();
			ret.setId(-1l);
			return ret;
		} catch (InstantiationException ex) {
			LOGGER.log(Level.SEVERE, null, ex);
			throw new RuntimeException(ex);
		} catch (IllegalAccessException ex) {
			LOGGER.log(Level.SEVERE, null, ex);
			throw new RuntimeException(ex);
		}
	}

	protected abstract EntityFacadeLocal<T> getFacade();

	// Converter for entity 
	public static class EntityControllerConverter implements Converter {

		private final String entityControllerName;

		public EntityControllerConverter(String entityControllerName) {
			this.entityControllerName = entityControllerName;
		}

		@Override
		public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
			if (value == null || value.length() == 0) {
				return null;
			}

			@SuppressWarnings("rawtypes")
			EntityController controller = (EntityController) facesContext.getApplication().getELResolver()
					.getValue(facesContext.getELContext(), null, entityControllerName);
			if ("-1".equals(value)) {
				System.out.println("Retrieving selection for -1");
				Thread.dumpStack();
			}
			return controller.getEntity(getKey(value));
		}

		java.lang.Long getKey(String value) {
			java.lang.Long key;
			key = Long.valueOf(value);
			return key;
		}

		String getStringKey(java.lang.Long value) {
			return Long.toString(value, 10);
		}

		@Override
		public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
			if (object == null) {
				return null;
			}
			if (object instanceof EntityId) {
				EntityId o = (EntityId) object;
				return getStringKey(o.getId());
			} else if (object instanceof Long) {
				Long l = ((Long) object);
				return Long.toString(l, 10);
			} else {
				throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName()
						+ "; expected type: " + EntityId.class.getName());
			}
		}

	}

}
