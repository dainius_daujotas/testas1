package testas.entities;

import static testas.entities.Game.BASKETBALL;
import static testas.entities.Game.FOOTBALL;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dainius
 */
public enum Position {
	// Footbal positions
	GOALKEEPER(FOOTBALL), DEFENDER(FOOTBALL), MIDFIELDER(FOOTBALL), STRIKER(FOOTBALL),
	// Basketball positions
	GUARD(BASKETBALL), FORWARD(BASKETBALL), CENTER(BASKETBALL);

	private final Game game;

	private Position(Game game) {
		this.game = game;
	}

	public Game getGame() {
		return game;
	}

	private static final Map<Game, List<Position>> positions;

	static {
		Map<Game, List<Position>> tmp = new HashMap<>();
		List<Position> pos = Arrays.asList(GOALKEEPER, DEFENDER, MIDFIELDER, STRIKER);
		tmp.put(FOOTBALL, pos);
		pos = Arrays.asList(GUARD, FORWARD, CENTER);
		tmp.put(BASKETBALL, pos);
		positions = Collections.unmodifiableMap(tmp);
	}

	public static List<Position> positions(Game game) {
		return game != null ? positions.get(game) : Collections.<Position> emptyList();
	}
}
