package testas.services.impl;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import testas.services.EntityFacadeLocal;
import testas.services.SortCriteria;

/**
 *
 * @author dainius
 */
public abstract class AbstractFacade<T> implements EntityFacadeLocal<T> {

    protected final Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
    
    
    protected abstract EntityManager getEntityManager();

    @Override
    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    @Override
    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    @Override
    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    @Override
    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    @Override
    public List<T> findAll() {
        CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder()
        		.createQuery(entityClass);
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    @Override
    public List<T> findRange(int from, int limit) {
        CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder()
        		.createQuery(entityClass);
        cq.select(cq.from(entityClass));
        TypedQuery<T> q = getEntityManager().createQuery(cq);
        q.setMaxResults(limit);
        q.setFirstResult(from);
        return q.getResultList();
    }
    
    
    @Override
    public List<T> findRange(int from, int limit, SortCriteria sortCriteria,
            Map<String, Object> filters) {
        final CriteriaBuilder criteriaBuilder
                = getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<T> cq = criteriaBuilder.createQuery(entityClass);
        final Root<T> root = cq.from(entityClass);
        cq.select(root);
        if (filters != null && !filters.isEmpty()) {
            Predicate[] predicates = new Predicate[filters.size()];
            int i = 0;
            for (Map.Entry<String, Object> filter : filters.entrySet()) {
                predicates[i++] = criteriaBuilder.equal(
                        root.get(filter.getKey()),
                        filter.getValue()
                );
            }
            cq.where(criteriaBuilder.and(predicates));
        }
        if (sortCriteria != null) {
            final Path<?> field = root.get(sortCriteria.getField());
            final Order order = sortCriteria.isAscending()
                    ? criteriaBuilder.asc(field)
                    : criteriaBuilder.desc(field);
            cq.orderBy(order);
        }
        TypedQuery<T> q = getEntityManager().createQuery(cq);
        q.setMaxResults(limit);
        q.setFirstResult(from);
        return q.getResultList();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
    public int count() {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery cq = criteriaBuilder
        		.createQuery();
		Root<T> rt = cq.from(entityClass);
        Expression<Long> select = criteriaBuilder.count(rt);
        cq.select(select);
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
