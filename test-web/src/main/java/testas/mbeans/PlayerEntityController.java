package testas.mbeans;

import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

import testas.entities.Game;
import testas.entities.PlayerEntity;
import testas.entities.Position;
import testas.entities.TeamEntity;
import testas.services.PlayerEntityFacadeLocal;
import testas.services.TeamEntityFacadeLocal;

@Named("playerEntityController")
@SessionScoped
public class PlayerEntityController extends EntityController<PlayerEntity> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2335722051700992731L;
	@EJB
	private PlayerEntityFacadeLocal ejbFacade;

	@EJB
	private TeamEntityFacadeLocal teamsFacade;

	public PlayerEntityController() {
		super(PlayerEntity.class);
	}

	protected PlayerEntityFacadeLocal getFacade() {
		return ejbFacade;
	}

	@FacesConverter(forClass = PlayerEntity.class)
	public static class PlayerEntityControllerConverter extends EntityControllerConverter {

		public PlayerEntityControllerConverter() {
			super("playerEntityController");
		}
	}

	public List<Position> getPossiblePositions() {
		return Position.positions(getEditing().getGame());
	}

	public List<TeamEntity> getPossibleTeams() {
		final Game game = getEditing().getGame();
		return game != null ? teamsFacade.teamsForGame(game) : Collections.<TeamEntity> emptyList();
	}

	public int getMaxPlayedGames() {
		return getEditing().getTeam() != null ? getEditing().getTeam().getPlayedGames() : 0;
	}
}
