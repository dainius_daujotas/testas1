package testas.entities;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author dainius
 */
@Entity
@Table(
        name = "leagues",
        uniqueConstraints = {
            @UniqueConstraint(
                    name = "leagues_by_game_name",
                    columnNames = {"game", "name"}
            )
        }
)
@NamedQueries(value = {
    @NamedQuery(
            name = LeagueEntity.getLeaguesByGame,
            query = "select entity from LeagueEntity entity "
                    + "where entity.game = :game order by entity.name"
    )

})
@NamedNativeQueries(value = {
    @NamedNativeQuery(
            name = LeagueEntity.getTopLeagues,
            resultClass = LeagueEntity.class,
            query = "select distinct "
            + "	  l.id as id, "
            + "    l.game as game, "
            + "    l.name as name "
            + "from leagues l "
            + "left join teams_leagues tl "
            + "	left join players p "
            + "  on p.team_id = tl.teams_id "
            + "on tl.leagues_id = l.id "
            + "where l.game = :game "
            + "group by l.id, l.game, l.name	"
            + "order by sum(p.points) desc"
    ),
    @NamedNativeQuery(
            name = LeagueEntity.getTopLeaguesAvg,
            resultClass = LeagueEntity.class,
            query = "select distinct "
            + "	l.id as id, "
            + "	l.game as game, "
            + "	l.name as name, "
            + "	sum(p.points) "
            + "from leagues l "
            + "left join teams_leagues tl "
            + "	left join teams t "
            + "		left join players p on p.team_id = t.id "
            + "	on t.playedGames > 0 and t.id = tl.teams_id "
            + "on tl.leagues_id = l.id "
            + "where l.game = :game "
            + "group by l.id, l.game, l.name "
            + "order by sum(p.points)/sum(t.playedGames) desc "
    )

})
public class LeagueEntity implements EntityId, Serializable {

    public static final String getLeaguesByGame = "LeagueEntity.getLeaguesByGame";
    public static final String getTopLeagues = "LeagueEntity.getTopLeagues";
    public static final String getTopLeaguesAvg = "LeagueEntity.getTopLeaguesAvg";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private Game game;

    @ManyToMany(mappedBy = "leagues")
    private Set<TeamEntity> teams;

    public LeagueEntity() {
    }

    public LeagueEntity(String name, Game game) {
        this.name = name;
        this.game = game;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Set<TeamEntity> getTeams() {
        return teams;
    }

    public void setTeams(Set<TeamEntity> teams) {
        this.teams = teams;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof LeagueEntity)) {
            return false;
        }
        LeagueEntity other = (LeagueEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LeagueEntity [id=" + id + ", name=" + name + ", game=" + game + "]";
    }

}
