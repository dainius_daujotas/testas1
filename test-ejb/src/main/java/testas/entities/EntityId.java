package testas.entities;

/**
 *
 * @author dainius
 */
public interface EntityId {
    public Long getId();
    public void setId(Long id);
}
