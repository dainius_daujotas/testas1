/**
 * 
 */

function DataTableExt(widgetVar) {
	var widget = PF(widgetVar);
	var editingRow = null;
	var editing = false;

	var savedRow = null;
	var savedSilent = null;

	widget.editSelectedRow = function() {
		if (widget.selection.length === 0)
			return;
		var row = widget.findRow(widget.originRowIndex);
		if (row[0].dataset.rk !== widget.selection[0]) {
			row = row.prevObject.find("tr[data-rk="+widget.selection[0]+"]");
			widget.selectRow(row, true);
			editingRow = row;
		} else
			editingRow = row;
		widget.switchToRowEdit(editingRow);
		editing = true;
	}

	widget.oldSelectRow = widget.selectRow;

	widget.selectRow = function(row, silent) {
		if (editing) {
			savedRow = row;
			savedSilent = silent;
			PF("rowEditingConfirm").show();
		}
		widget.oldSelectRow(row, silent);
	}

	widget.saveAndResumeSelect = function() {
		PF("rowEditingConfirm").hide();
		widget.saveEditedRow();
		widget.oldSelectRow(savedRow, savedSilent);
		savedRow = savedSilent = null;
	}

	widget.cancelAndResumeSelect = function() {
		PF("rowEditingConfirm").hide();
		widget.cancelEditedRow();
		widget.oldSelectRow(savedRow, savedSilent);
		savedRow = savedSilent = null;
	}

	widget.saveEditedRow = function() {
		if (editing) {
			widget.saveRowEdit(editingRow);
			editinRow = null;
			editing = false;
		}
	}

	widget.cancelEditedRow = function() {
		if (editing) {
			widget.cancelRowEdit(editingRow);
			editinRow = null;
			editing = false;
		}
	}
	
	widget.editRow = function(num) {
		var row = widget.findRow(widget.originRowIndex);
		widget.selectRow(row, false);
		editingRow = row;
		widget.switchToRowEdit(editingRow);
		editing = true;

	}
}

function processRowChangeComplete(obj) {
	console.log("Complete");
	console.log(obj);
}


function processRowChangeError(obj) {
	console.log("Error");
	console.log(obj);
}
