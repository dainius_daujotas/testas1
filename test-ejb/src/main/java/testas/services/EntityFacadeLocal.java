/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testas.services;

import java.util.List;
import java.util.Map;

/**
 *
 * @author dainius
 */
public interface EntityFacadeLocal<T> {

    void create(T leagueEntity);

    void edit(T leagueEntity);

    void remove(T leagueEntity);

    T find(Object id);

    List<T> findAll();

    List<T> findRange(int from, int limit);

    List<T> findRange(int from, int limit, SortCriteria sortCriteria,
            Map<String, Object> filters);

    int count();

}
