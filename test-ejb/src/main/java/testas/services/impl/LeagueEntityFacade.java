/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testas.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import testas.entities.Game;
import testas.entities.LeagueEntity;
import testas.entities.TeamEntity;
import testas.services.LeagueEntityFacadeLocal;

/**
 *
 * @author dainius
 */
@Stateless
public class LeagueEntityFacade extends AbstractFacade<LeagueEntity> 
        implements LeagueEntityFacadeLocal {

    @PersistenceContext(unitName = "Test-PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LeagueEntityFacade() {
        super(LeagueEntity.class);
    }

	@Override
	public List<TeamEntity> getTeams(long leagueId) {
		LeagueEntity league = em.find(LeagueEntity.class, leagueId);
		return new ArrayList<TeamEntity>(league.getTeams());
	}

	@Override
	public List<LeagueEntity> getLeagues(Game game) {
		return new ArrayList<>(em.createNamedQuery(LeagueEntity.getLeaguesByGame, LeagueEntity.class)
			.setParameter("game", game)
			.getResultList());
	}

	@Override
	public List<LeagueEntity> getTopLeagues(Game game, int count) {
		return new ArrayList<>(em.createNamedQuery(LeagueEntity.getTopLeagues, LeagueEntity.class)
				.setParameter("game", game.ordinal())
				.setMaxResults(count)
				.getResultList());
	}

	@Override
	public List<LeagueEntity> getTopLeaguesByAvg(Game game, int count) {
		return new ArrayList<>(em.createNamedQuery(LeagueEntity.getTopLeaguesAvg, LeagueEntity.class)
				.setParameter("game", game.ordinal())
				.setMaxResults(count)
				.getResultList());
	}
    
    
}
