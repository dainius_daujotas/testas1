package testas.mbeans.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import testas.entities.EntityId;
import testas.services.EntityFacadeLocal;
import testas.services.SortCriteria;

/**
 *
 * @author dainius
 */
public class EntitiesDataModel<T extends EntityId> extends LazyDataModel<T> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2638855839360463746L;
	private T newItem;
	private final EntityFacadeLocal<T> facade;

	public EntitiesDataModel(EntityFacadeLocal<T> facade) {
		this.facade = facade;
		setRowCount(facade.count());
	}

	public EntitiesDataModel(T newItem, EntityFacadeLocal<T> facade) {
		this.newItem = newItem;
		this.facade = facade;
		setRowCount(facade.count() + 1);
	}

	// return new ListDataModel(
	// getFacade().findRange(
	// new int[]{
	// getPageFirstItem(),
	// getPageFirstItem() + getPageSize()
	// }
	// )
	// );
	@Override
	public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		SortCriteria sc;
		if (sortField != null) {
			switch (sortOrder) {
			case ASCENDING:
				sc = new SortCriteria(sortField, true);
				break;
			case DESCENDING:
				sc = new SortCriteria(sortField, false);
				break;
			default:
				sc = null;
			}
		} else {
			sc = null;
		}
		if (newItem != null) {
			int[] range = first == 0 ? new int[] { first, pageSize - 1 } : new int[] { first - 1, pageSize };
			List<T> tmp = facade.findRange(range[0], range[1], sc, filters);
			if (first == 0) {
				List<T> ret = new ArrayList<>();
				ret.add(newItem);
				ret.addAll(tmp);
				return ret;
			} else
				return tmp;
		} else {
			return facade.findRange(first, pageSize, sc, filters);
		}
	}

	@Override
	public Object getRowKey(T t) {
		return t.getId();
	}

	@Override
	public T getRowData(String string) {
		return facade.find(new Long(string));
	}
}
