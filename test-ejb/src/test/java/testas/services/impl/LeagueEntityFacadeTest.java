/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testas.services.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import testas.entities.Game;
import testas.entities.LeagueEntity;
import testas.entities.PlayerEntity;
import testas.entities.Position;
import testas.entities.TeamEntity;
import testas.services.LeagueEntityFacadeLocal;

/**
 *
 * @author dainius
 */
@RunWith(Arquillian.class)
@Transactional
public class LeagueEntityFacadeTest {

    @PersistenceContext(unitName = "Test-PU")
    private EntityManager em;

    @EJB
    LeagueEntityFacadeLocal leagueEntityFacade;

    public LeagueEntityFacadeTest() {
    }

    @Deployment
    public static EnterpriseArchive createTestArchive() {
        try {
            File[] files = Maven.configureResolver().resolve("mysql:mysql-connector-java:5.1.37")
                    .withTransitivity()
                    .asFile();

            JavaArchive archive = ShrinkWrap.create(JavaArchive.class, "test-ejb.jar")
                    .addPackages(true, "testas.services", "testas.entities")
                    .addAsResource("persistence.xml", "META-INF/persistence.xml")
                    .addAsResource("jboss.xml", "META-INF/jboss.xml")
                    .addAsResource("datasource-ds.xml", "META-INF/datasource-ds.xml");

            EnterpriseArchive ear = ShrinkWrap.create(EnterpriseArchive.class, "test.ear")
                    .addAsModule(archive);

            for (File file : files) {
                ear.addAsLibrary(file, file.getName());
            }
            ear.addAsResource("application.xml", "application.xml")
                    .addAsResource("jboss-app.xml", "jboss-app.xml");

            return ear;
        } catch (RuntimeException ex) {
            throw ex;
        }

//        return null;
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testCreate() throws Exception {
        System.out.println("create");
        LeagueEntity entity;
        entity = new LeagueEntity("testas-name", Game.FOOTBALL);
        leagueEntityFacade.create(entity);

        assertNotNull("Invalid id after creation", entity.getId());
        assertEquals(
                "Expected result",
                entity,
                em.find(LeagueEntity.class, entity.getId())
        );
    }

    /**
     * Test of edit method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testEdit() throws Exception {
        System.out.println("edit");
        LeagueEntity entity = new LeagueEntity("testas", Game.BASKETBALL);
        em.persist(entity);
        long id = entity.getId();
        entity = em.find(LeagueEntity.class, id);
        assertNotNull("Test initialization failed", entity);
        entity.setName("testas2");
        leagueEntityFacade.edit(entity);
        entity = em.find(LeagueEntity.class, id);
        assertEquals(
                "Name must be updated",
                "testas2", entity.getName()
        );
        em.remove(entity);
    }

    /**
     * Test of remove method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testRemove() throws Exception {
        System.out.println("remove");
        LeagueEntity entity = new LeagueEntity("testas", Game.FOOTBALL);
        em.persist(entity);
        assertNotNull(
                "Test initialization failed",
                em.find(LeagueEntity.class, entity.getId())
        );
        leagueEntityFacade.remove(entity);
        assertNull(
                "Entity must be removed",
                em.find(LeagueEntity.class, entity.getId())
        );
    }

    /**
     * Test of find method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testFind() throws Exception {
        System.out.println("find");
        LeagueEntity expResult = new LeagueEntity("testas", Game.FOOTBALL);
        em.persist(expResult);
        Object id = expResult.getId();

        LeagueEntity result = leagueEntityFacade.find(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of findAll method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testFindAll() throws Exception {
        System.out.println("findAll");
        assertTrue(
                "Invalid initial state",
                leagueEntityFacade.findAll().isEmpty()
        );
        List<LeagueEntity> expResult = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            LeagueEntity entity = new LeagueEntity(
                    "Item_" + i,
                    Game.values()[i & 1]
            );
            em.persist(entity);
            expResult.add(entity);
        }
        List<LeagueEntity> result = leagueEntityFacade.findAll();
        assertEquals(
                "Sizes of lists must be same",
                expResult.size(), result.size()
        );
        assertTrue(
                "And contain all elements",
                result.containsAll(expResult)
        );

    }

    /**
     * Test of findRange method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testFindRange_intArr() throws Exception {
        System.out.println("findRange");
        for (int i = 0; i < 10; i++) {
            LeagueEntity entity = new LeagueEntity(
                    "Item_" + i,
                    Game.values()[i & 1]
            );
            em.persist(entity);
        }

        CriteriaQuery<LeagueEntity> cq = em.getCriteriaBuilder()
                .createQuery(LeagueEntity.class);
        cq.select(cq.from(LeagueEntity.class));
        List<LeagueEntity> expResult = em.createQuery(cq)
                .setFirstResult(1)
                .setMaxResults(5)
                .getResultList();
        List<LeagueEntity> result = leagueEntityFacade.findRange(1, 5);
        assertEquals(
                "Sizes of lists must be same",
                expResult.size(), result.size()
        );
        assertTrue(
                "And contain all elements",
                result.containsAll(expResult)
        );

    }

    /**
     * Test of findRange method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testFindRange_3args() throws Exception {
        System.out.println("findRange");
        for (int i = 0; i < 10; i++) {
            LeagueEntity entity = new LeagueEntity(
                    "Item_" + i,
                    Game.values()[i & 1]
            );
            em.persist(entity);
        }

        CriteriaQuery<LeagueEntity> cq = em.getCriteriaBuilder()
                .createQuery(LeagueEntity.class);
        cq.select(cq.from(LeagueEntity.class));
        List<LeagueEntity> expResult = em.createQuery(cq)
                .setFirstResult(1)
                .setMaxResults(5)
                .getResultList();
        List<LeagueEntity> result = leagueEntityFacade
                .findRange(1, 5, null, Collections.<String, Object>emptyMap());
        assertEquals(
                "Sizes of lists must be same",
                expResult.size(), result.size()
        );
        assertTrue(
                "And contain all elements",
                result.containsAll(expResult)
        );
    }

    /**
     * Test of count method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testCount() throws Exception {
        System.out.println("count");
//        int expResult = 0;
        int result;
        for (int expResult = 0; expResult < 10; em.persist(new LeagueEntity("asdfasdf" + (expResult++), Game.FOOTBALL))) {
            result = leagueEntityFacade.count();
            assertEquals(expResult, result);
        }
    }

    /**
     * Test of getTeams method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testGetTeams() throws Exception {
        System.out.println("getTeams");
        LeagueEntity leagueEntity = new LeagueEntity("tesatas", Game.FOOTBALL);
        leagueEntity.setTeams(new HashSet<>());
        em.persist(leagueEntity);
        long leagueId = leagueEntity.getId();
        List<TeamEntity> expResult = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            TeamEntity teamEntity = new TeamEntity("name_" + i, Game.FOOTBALL, 0);
            HashSet<LeagueEntity> leagues = new HashSet<>();
            leagues.add(leagueEntity);
            teamEntity.setLeagues(leagues);
            leagueEntity.getTeams().add(teamEntity);
            em.persist(teamEntity);
            expResult.add(teamEntity);
        }

        List<TeamEntity> result = leagueEntityFacade.getTeams(leagueId);
        assertEquals(
                "Sizes of lists must be same",
                expResult.size(), result.size()
        );
        assertTrue(
                "And contain all elements",
                result.containsAll(expResult)
        );
    }

    /**
     * Test of getLeagues method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testGetLeagues() throws Exception {
        System.out.println("getLeagues");
        List<LeagueEntity> expResult = new ArrayList<>(5);
        final Game game = Game.BASKETBALL;
        for (int i = 0; i < 10; i++) {
            Game iGame = Game.values()[i & 1];
            LeagueEntity entity = new LeagueEntity(
                    "Item_" + i,
                    iGame
            );
            em.persist(entity);
            if (iGame == game) {
                expResult.add(entity);
            }
        }
        List<LeagueEntity> result = leagueEntityFacade.getLeagues(game);
        assertEquals(
                "Sizes of lists must be same",
                expResult.size(), result.size()
        );
        assertTrue(
                "And contain all elements",
                result.containsAll(expResult)
        );

    }

    /**
     * Test of getTopLeagues method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testGetTopLeagues() throws Exception {
        System.out.println("getTopLeagues");
        List<LeagueWrapper> data = initializeTopData();
        Game game = Game.BASKETBALL;
        int count = 3;
        List<LeagueWrapper> expResult = data.stream()
                .sorted(LEAGUE_POINTS_COMPARATOR)
                .limit(count)
                .collect(Collectors.<LeagueWrapper>toList());
        List<LeagueEntity> result = leagueEntityFacade.getTopLeagues(game, count);
        assertEquals(
                "Sizes of lists must be same",
                expResult.size(), result.size()
        );
        boolean ok = true;
        for (int i = 0; i < expResult.size(); i++) {

            ok = ok && expResult.get(i).getLeague().equals(result.get(i));
            System.out.println("EXP: " + expResult.get(i).getLeague());
            System.out.println("GOT: " + result.get(i));
        }
        assertTrue(
                "All items must be in same order",
                ok
        );
    }

    /**
     * Test of getTopLeaguesByAvg method, of class LeagueEntityFacade.
     */
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testGetTopLeaguesByAvg() throws Exception {
        System.out.println("getTopLeaguesByAvg");
        List<LeagueWrapper> data = initializeTopData();
        Game game = Game.BASKETBALL;
        int count = 3;
        List<LeagueWrapper> expResult = data.stream()
                .sorted(LEAGUE_AVG_POINTS_COMPARATOR)
                .limit(count)
                .collect(Collectors.<LeagueWrapper>toList());
        List<LeagueEntity> result = leagueEntityFacade.getTopLeaguesByAvg(game, count);
        assertEquals(
                "Sizes of lists must be same",
                expResult.size(), result.size()
        );
        boolean ok = true;
        for (int i = 0; i < expResult.size(); i++) {

            ok = ok && expResult.get(i).getLeague().equals(result.get(i));
            System.out.println("EXP: " + expResult.get(i).getLeague());
            System.out.println("GOT: " + result.get(i));
        }
        assertTrue(
                "All items must be in same order",
                ok
        );
    }

    private List<LeagueWrapper> initializeTopData() {
        List<LeagueWrapper> leagues = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            LeagueEntity league = new LeagueEntity("League_" + i, Game.BASKETBALL);
            league.setTeams(new HashSet<>());
            em.persist(league);
//            System.out.println(league);
            List<TeamWrapper> teams = new ArrayList<>();
            for (int j = 0; j < 5; j++) {
                TeamEntity team = new TeamEntity("Team_" + i + "_" + j, league.getGame(), (i + j) % 5);
                team.setLeagues(new HashSet<>());
                team.getLeagues().add(league);
                league.getTeams().add(team);
                em.persist(team);
//                System.out.println(team);
                List<PlayerEntity> players = new ArrayList<>();
                for (int k = 0; k < 8; k++) {
                    List<Position> positions = Position.positions(team.getGame());
                    int playedGames = team.getPlayedGames() == 0 ? 0
                            : (i + j + k) % team.getPlayedGames();
                    int maxPoints = 13;
                    int points = maxPoints == 0 ? 0
                            : ((i + j + k) % maxPoints) * playedGames;
                    PlayerEntity player = new PlayerEntity(
                            "firstName_" + "_" + i + "_" + j + "_" + k,
                            "lastName_" + "_" + i + "_" + j + "_" + k,
                            team.getGame(),
                            positions.get((i + j + k) % positions.size()),
                            team,
                            points,
                            playedGames
                    );
                    em.persist(player);
//                    System.out.println(player);
                    players.add(player);
                }
                teams.add(new TeamWrapper(team, players));
            }
            leagues.add(new LeagueWrapper(league, teams));
        }
        return leagues;
    }

    private static class TeamWrapper {

        private final TeamEntity team;
        private final List<PlayerEntity> players;
        private int points = -1;

        public TeamWrapper(TeamEntity team, List<PlayerEntity> players) {
            this.team = team;
            this.players = players;
        }

        public TeamEntity getTeam() {
            return team;
        }

        public List<PlayerEntity> getPlayers() {
            return players;
        }

        public int getPoints() {
            if (points >= 0) {
                return points;
            }
            for (PlayerEntity p : players) {
                points += p.getPoints();
            }
            return points;
        }
    }

    private static class LeagueWrapper {

        private final LeagueEntity league;
        private final List<TeamWrapper> teams;
        private int points = -1;
        private double avgPoints = -1;

        public LeagueWrapper(LeagueEntity league, List<TeamWrapper> teams) {
            this.league = league;
            this.teams = teams;
        }

        public LeagueEntity getLeague() {
            return league;
        }

        public List<TeamWrapper> getTeams() {
            return teams;
        }

        public int getPoints() {
            if (points >= 0) {
                return points;
            }
            points = teams.stream()
                    .mapToInt(TeamWrapper::getPoints)
                    .sum();
            return points;
        }

        public double getAvgPoints() {
            if (avgPoints >= 0) {
                return avgPoints;
            }
            int games = getTeams().stream()
                    .mapToInt(t -> t.getTeam().getPlayedGames())
                    .sum();
            avgPoints = ((double) getPoints()) / games;
            return avgPoints;
        }
    }

    private static final Comparator<LeagueWrapper> LEAGUE_POINTS_COMPARATOR = 
            (LeagueWrapper o1, LeagueWrapper o2) 
                    -> o2.getPoints() - o1.getPoints();

    private static final Comparator<LeagueWrapper> LEAGUE_AVG_POINTS_COMPARATOR = 
            (LeagueWrapper o1, LeagueWrapper o2) 
                    -> Double.compare(o2.getAvgPoints(), o1.getAvgPoints());
}
