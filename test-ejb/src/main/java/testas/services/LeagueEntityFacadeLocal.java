package testas.services;

import java.util.List;

import javax.ejb.Local;

import testas.entities.Game;
import testas.entities.LeagueEntity;
import testas.entities.TeamEntity;

/**
 *
 * @author dainius
 */
@Local
public interface LeagueEntityFacadeLocal extends EntityFacadeLocal<LeagueEntity>{
	List<TeamEntity> getTeams(long leagueId);

	List<LeagueEntity> getLeagues(Game game);

	List<LeagueEntity> getTopLeagues(Game game, int count);
  
	List<LeagueEntity> getTopLeaguesByAvg(Game game, int count);
}
