package testas.mbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

import org.primefaces.model.DualListModel;

import testas.entities.LeagueEntity;
import testas.entities.TeamEntity;
import testas.services.LeagueEntityFacadeLocal;
import testas.services.TeamEntityFacadeLocal;

@Named("teamEntityController")
@SessionScoped
public class TeamEntityController extends EntityController<TeamEntity> {
	
	private List<LeagueEntity> leagues;
	/**
	 * 
	 */
	private static final long serialVersionUID = -6046685050686569878L;
	@EJB
	private TeamEntityFacadeLocal ejbFacade;
	@EJB
	private LeagueEntityFacadeLocal leaguesFacade;

	private DualListModel<LeagueEntity> leaguesDl;

	public TeamEntityController() {
		super(TeamEntity.class);
	}

	protected TeamEntityFacadeLocal getFacade() {
		return ejbFacade;
	}

	@Override
	public String prepareCreate() {
		String ret = super.prepareCreate();
		getEditing().setLeagues(new HashSet<LeagueEntity>());
		List<LeagueEntity> src = Collections.<LeagueEntity> emptyList();
		List<LeagueEntity> target = new ArrayList<LeagueEntity>();
		leaguesDl = new DualListModel<LeagueEntity>(src, target);
		return ret;
	}

	@Override
	public String prepareEdit() {
		String ret = super.prepareEdit();
		List<LeagueEntity> src = leaguesFacade.getLeagues(getEditing().getGame());
		List<LeagueEntity> target = new ArrayList<LeagueEntity>(ejbFacade.getLeagues(getSelected().getId()));
		src.removeAll(target);
		leaguesDl = new DualListModel<LeagueEntity>(src, target);
		return ret;
	}

	@Override
	public void setSelected(TeamEntity newVal) {
		leaguesDl = null;
		leagues = null;
		super.setSelected(newVal);
	}
	
	public List<LeagueEntity> getLeagues() {
		if (leagues == null) {
			if (getEditing()!=null && getEditing().getId()!=null) {
				leagues = ejbFacade.getLeagues(getEditing().getId());
			} else {
				leagues = Collections.emptyList();
			}
		}
		return leagues;
	}
	
	public void gameValueChanged() {
		if (getEditing().getId() != null && -1l != getEditing().getId()) {
			throw new IllegalStateException("Updating game on already created team");
		}
		List<LeagueEntity> src = getEditing().getGame() == null ? Collections.<LeagueEntity> emptyList()
				: leaguesFacade.getLeagues(getEditing().getGame());

		leaguesDl = new DualListModel<LeagueEntity>(src, new ArrayList<LeagueEntity>());

	}

	public DualListModel<LeagueEntity> getLeaguesDualList() {
		return leaguesDl;
	}

	public void setLeaguesDualList(DualListModel<LeagueEntity> dl) {
		this.leaguesDl = dl;
	}

	@Override
	public String create() {
		getEditing().getLeagues().addAll(leaguesDl.getTarget());
		getEditing().getLeagues().retainAll(leaguesDl.getTarget());
		return super.create();
	}
	
	@Override
	public String update() {
		getEditing().setLeagues(new HashSet<>(leaguesDl.getTarget()));
		return super.update();
	}
	
	@FacesConverter(forClass = TeamEntity.class)
	public static class TeamEntityControllerConverter extends EntityControllerConverter {

		public TeamEntityControllerConverter() {
			super("teamEntityController");
		}

	}

}
