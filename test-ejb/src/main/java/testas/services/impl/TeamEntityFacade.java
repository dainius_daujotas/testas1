/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testas.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import testas.entities.Game;
import testas.entities.LeagueEntity;
import testas.entities.TeamEntity;
import testas.services.TeamEntityFacadeLocal;

/**
 *
 * @author dainius
 */
@Stateless
public class TeamEntityFacade extends AbstractFacade<TeamEntity> implements TeamEntityFacadeLocal {

	@PersistenceContext(unitName = "Test-PU")
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public TeamEntityFacade() {
		super(TeamEntity.class);
	}

	@Override
	public List<LeagueEntity> getLeagues(long id) {
		return new ArrayList<>(em.find(entityClass, id).getLeagues());
	}

	@Override
	public List<TeamEntity> teamsForGame(Game game) {
		return new ArrayList<>(em.createNamedQuery(TeamEntity.getTeamsByGame, TeamEntity.class)
				.setParameter("game", game).getResultList());

	}

	@Override
	public List<TeamEntity> topTeams(Game game, int count) {
		return new ArrayList<>(em.createNamedQuery(TeamEntity.getTopTeamsByPlayerPoints, TeamEntity.class)
				.setParameter("game", game.ordinal())
				.setMaxResults(count)
				.getResultList());
	}

	@Override
	public List<TeamEntity> topTeamsByAvg(Game game, int count) {
		return new ArrayList<>(em.createNamedQuery(TeamEntity.getTopTeamsByPlayerPointsAvg, TeamEntity.class)
				.setParameter("game", game.ordinal())
				.setMaxResults(count)
				.getResultList());
	}

	
}
