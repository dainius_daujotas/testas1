package testas.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dainius
 */
@Entity
@Table(name = "players")
@NamedQueries(value = {
    @NamedQuery(
            name = PlayerEntity.topPlayers,
            query = "select entity from PlayerEntity entity "
            + "where entity.game = :game order by entity.points desc"
    ),
    @NamedQuery(
            name = PlayerEntity.topPlayersByAvg,
            query = "select entity "
            + "from PlayerEntity entity "
            + "where "
            + "	   entity.game = :game "
            + "and entity.playedGames > 0 "
            + "order by entity.points/entity.playedGames desc"
    )
})
public class PlayerEntity implements EntityId, Serializable {

    public static final String topPlayers = "PlayerEntity.top10Players";
    public static final String topPlayersByAvg = "PlayerEntity.top10PlayersByAvg";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private Game game;
    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private Position position;
    @ManyToOne
    private TeamEntity team;
    private int points;
    private int playedGames;

    public PlayerEntity() {
    }

    public PlayerEntity(String firstName, String lastName, Game game, Position position, TeamEntity team, int points, int playedGames) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.game = game;
        this.position = position;
        this.team = team;
        this.points = points;
        this.playedGames = playedGames;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
        if (team != null && team.getGame() != game) {
            team = null;
        }
        if (position != null && position.getGame() != game) {
            position = null;
        }
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
        if (position != null && game != null && game != position.getGame()) {
            throw new IllegalArgumentException("Expected position for different game");
        }
    }

    public TeamEntity getTeam() {
        return team;
    }

    public void setTeam(TeamEntity team) {
        this.team = team;
        if (team != null) {
            if (game != null && team.getGame() != game) {
                throw new IllegalArgumentException("Expected team for different game");
            }
            setPlayedGames(Math.min(team.getPlayedGames(), playedGames));
        }
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        if (playedGames == 0 && points > 0) {
            throw new IllegalArgumentException("Got points without a single game played ?");
        }
        this.points = points;
    }

    public int getPlayedGames() {
        return playedGames;
    }

    public void setPlayedGames(int playedGames) {
        this.playedGames = playedGames;
        if (playedGames == 0) {
            setPoints(0);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof PlayerEntity)) {
            return false;
        }
        PlayerEntity other = (PlayerEntity) object;
        if ((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PlayerEntity["
                + "id=" + id
                + ", firstName=" + firstName
                + ", lastName=" + lastName
                + ", game=" + game
                + ", position=" + position
                + ", points=" + points
                + ", playedGames=" + playedGames
                + '}';
    }

}
