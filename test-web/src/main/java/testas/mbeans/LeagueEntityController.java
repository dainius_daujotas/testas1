package testas.mbeans;

import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

import testas.entities.LeagueEntity;
import testas.entities.TeamEntity;
import testas.services.LeagueEntityFacadeLocal;

@Named("leagueEntityController")
@SessionScoped
public class LeagueEntityController extends EntityController<LeagueEntity> {

	private List<TeamEntity> teams;

	/**
	 * 
	 */
	private static final long serialVersionUID = 5088633848424830957L;
	@EJB
	private LeagueEntityFacadeLocal ejbFacade;

	public LeagueEntityController() {
		super(LeagueEntity.class);
	}

	@Override
	protected LeagueEntityFacadeLocal getFacade() {
		return ejbFacade;
	}

	@Override
	public void setSelected(LeagueEntity league) {
		
		super.setSelected(league);
		teams = null;
	}
	

	public List<TeamEntity> getTeams() {
		if (teams == null) {
			if (getSelected().getId() != null) {
				teams = ejbFacade.getTeams(getSelected().getId());
			} else {
				teams = Collections.emptyList();
			}
		}
		return teams;
	}

	
	
	@FacesConverter(forClass = LeagueEntity.class, value = "leagueConverter")
	public static class LeagueEntityControllerConverter extends EntityControllerConverter {

		public LeagueEntityControllerConverter() {
			super("leagueEntityController");
		}

	}

}
