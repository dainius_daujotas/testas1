package testas.services;

/**
 *
 * @author dainius
 */
public class SortCriteria {

    private final String field;
    private final boolean ascending;

    public SortCriteria(String field, boolean ascending) {
        this.field = field;
        this.ascending = ascending;
    }

    public String getField() {
        return field;
    }

    public boolean isAscending() {
        return ascending;
    }

}
