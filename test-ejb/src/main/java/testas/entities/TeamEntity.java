package testas.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author dainius
 */
@Entity
@Table(
        name = "teams",
        uniqueConstraints = {
            @UniqueConstraint(
                    name = "temas_game_name",
                    columnNames = {"game", "name"}
            )
        }
)
@NamedQueries(value = {
    @NamedQuery(
            name = TeamEntity.getTeamsByGame,
            query = "select entity from TeamEntity entity where entity.game = :game order by entity.name"
    )
/*Hibernate generuoja sql kurio nekramto MySQL
		,  
		@NamedQuery( 
				name=TeamEntity.getTopTeamsByPlayerPoints,
				query="select distinct entity.team "
						+ "from PlayerEntity entity "
						+ "where entity.game = :game "
						+ "group by entity.team.id, entity.team.game, entity.team.name, entity.team.playedGames "
						+ "order by max(entity.points) desc"
		)
 */
})
@NamedNativeQueries(value = {
    @NamedNativeQuery(
            name = TeamEntity.getTopTeamsByPlayerPoints,
            query = "select distinct "
            + "    teamentity1_.id as id, "
            + "    teamentity1_.game as game, "
            + "    teamentity1_.name as name, "
            + "    teamentity1_.playedGames as playedGames "
            + "from players playerenti0_ "
            + "    inner join teams teamentity1_ "
            + "    on playerenti0_.team_id=teamentity1_.id "
            + "where playerenti0_.game = :game "
            + "group by teamentity1_.id , "
            + "    teamentity1_.game , "
            + "    teamentity1_.name , "
            + "    teamentity1_.playedGames "
            + "order by max(playerenti0_.points) desc",
            resultClass = TeamEntity.class
    ),
    @NamedNativeQuery(
            name = TeamEntity.getTopTeamsByPlayerPointsAvg,
            query = "select "
            + "	t.id as id, "
            + "    t.game as game, "
            + "    t.name as name, "
            + "    t.playedGames as playedGames "
            + "from "
            + "	teams t "
            + "left join players p "
            + "on p.team_id = t.id "
            + "where  "
            + "	t.game = :game "
            + "and	t.playedGames > 0 "
            + "group by "
            + "	t.id, t.game, t.name, t.playedGames "
            + "order by (sum(p.points)/t.playedGames) desc ",
            resultClass = TeamEntity.class
    )
})
public class TeamEntity implements EntityId, Serializable {

    public static final String getTeamsByGame = "TeamEntity.getTeamsByGame";
    public static final String getTopTeamsByPlayerPoints = "TeamEntity.getTopTeamsByPlayerPoints";
    public static final String getTopTeamsByPlayerPointsAvg = "TeamEntity.getTopTeamsByPlayerPointsAvg";
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private Game game;
    @ManyToMany
    private Set<LeagueEntity> leagues;
    @Column(nullable = false)
    private int playedGames;

    public TeamEntity() {
    }

    public TeamEntity(String name, Game game, int playedGames) {
        this.name = name;
        this.game = game;
        this.playedGames = playedGames;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Set<LeagueEntity> getLeagues() {
        return leagues;
    }

    public void setLeagues(Set<LeagueEntity> leagues) {
        this.leagues = leagues;
    }

    public int getPlayedGames() {
        return playedGames;
    }

    public void setPlayedGames(int playedGames) {
        this.playedGames = playedGames;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof TeamEntity)) {
            return false;
        }
        TeamEntity other = (TeamEntity) object;
        if ((this.id == null && other.id != null) 
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "testas.entities.TeamEntity[ id=" + id + " ]";
    }

}
