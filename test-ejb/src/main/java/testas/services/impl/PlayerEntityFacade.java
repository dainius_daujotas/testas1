/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testas.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import testas.entities.Game;
import testas.entities.PlayerEntity;
import testas.services.PlayerEntityFacadeLocal;

/**
 *
 * @author dainius
 */
@Stateless
public class PlayerEntityFacade extends AbstractFacade<PlayerEntity> implements PlayerEntityFacadeLocal {

    @PersistenceContext(unitName = "Test-PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PlayerEntityFacade() {
        super(PlayerEntity.class);
    }

	@Override
	public List<PlayerEntity> topPlayers(Game game, int count) {
		return new ArrayList<>(em.createNamedQuery(PlayerEntity.topPlayers, PlayerEntity.class)
				.setParameter("game", game)
				.setMaxResults(count)
				.getResultList());
	}

	@Override
	public List<PlayerEntity> topPlayersByAvg(Game game, int count) {
		return new ArrayList<>(em.createNamedQuery(PlayerEntity.topPlayersByAvg, PlayerEntity.class)
				.setParameter("game", game)
				.setMaxResults(count)
				.getResultList());
	}
    
}
