package testas.services;

import java.util.List;

import javax.ejb.Local;

import testas.entities.Game;
import testas.entities.PlayerEntity;

/**
 *
 * @author dainius
 */
@Local
public interface PlayerEntityFacadeLocal extends EntityFacadeLocal<PlayerEntity> {

	List<PlayerEntity> topPlayers(Game game, int count);

	List<PlayerEntity> topPlayersByAvg(Game game, int count);


}
