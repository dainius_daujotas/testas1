package testas.mbeans;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import testas.entities.Game;
import testas.entities.LeagueEntity;
import testas.entities.PlayerEntity;
import testas.entities.TeamEntity;
import testas.services.LeagueEntityFacadeLocal;
import testas.services.PlayerEntityFacadeLocal;
import testas.services.TeamEntityFacadeLocal;

@Named("reportsController")
@RequestScoped
public class ReportsController {
	@EJB
	private PlayerEntityFacadeLocal playerFacade;
	@EJB
	private TeamEntityFacadeLocal teamFacade;
	@EJB
	private LeagueEntityFacadeLocal leagueFacade;
	
	public List<PlayerEntity> getBasketballTop10Players() {
		return playerFacade.topPlayers(Game.BASKETBALL, 10);
	}
	public List<TeamEntity> getBasketballTop10Teams() {
		return teamFacade.topTeams(Game.BASKETBALL, 10);
	}
	public List<LeagueEntity> getBasketballTop10Leagues() {
		return leagueFacade.getTopLeagues(Game.BASKETBALL, 10);
	}

	public List<PlayerEntity> getFootballTop10Players() {
		return playerFacade.topPlayersByAvg(Game.FOOTBALL, 10);
	}
	public List<TeamEntity> getFootballTop10Teams() {
		return teamFacade.topTeamsByAvg(Game.FOOTBALL, 10);
	}
	public List<LeagueEntity> getFootballTop10Leagues() {
		return leagueFacade.getTopLeaguesByAvg(Game.FOOTBALL, 10);
	}
}
