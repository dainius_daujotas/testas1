package testas.services;

import java.util.List;

import javax.ejb.Local;

import testas.entities.Game;
import testas.entities.LeagueEntity;
import testas.entities.TeamEntity;

/**
 *
 * @author dainius
 */
@Local
public interface TeamEntityFacadeLocal extends EntityFacadeLocal<TeamEntity>{

	List<LeagueEntity> getLeagues(long id);

	List<TeamEntity> teamsForGame(Game game);

	List<TeamEntity> topTeams(Game game, int count);
	List<TeamEntity> topTeamsByAvg(Game game, int count);

}
